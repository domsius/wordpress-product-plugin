<?php


/**
 * Plugin Name: Domsius
 * Version: 1.0.0
 * Description: Custom plugin
 * Author: Dominykas Luksys
 * 
 * 
 */

define( 'PLUGIN_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );
define( 'PLUGIN_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );


require_once PLUGIN_PATH . 'includes/setup.php';