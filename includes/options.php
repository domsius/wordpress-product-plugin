<?php

if ( class_exists( 'CSF' ) ) :

    $prefix = 'plugin_settings';

    CSF::createOptions( $prefix, array(

        'framework_title'         => 'Plugin settings',

        // menu settings
        'menu_title'              => 'Plugin settings',
        'menu_slug'               => 'plugin-settings',
        'menu_capability'         => 'manage_options',
        'menu_icon'               => 'dashicons-admin-generic',
        'menu_position'           => null,
        'menu_hidden'             => false,

        // menu extras
        'show_bar_menu'           => true,
        'show_sub_menu'           => false,
        'show_network_menu'       => true,
        'show_in_customizer'      => false,

        'show_search'             => false,
        'show_reset_all'          => false,
        'show_reset_section'      => false,
        'show_footer'             => true,
        'show_all_options'        => true,
        'sticky_header'           => true,
        'save_defaults'           => true,
        'ajax_save'               => true,

        // admin bar menu settings
        'admin_bar_menu_icon'     => 'dashicons-admin-generic',
        'admin_bar_menu_priority' => 80,

        // database model
        'transient_time'          => 0,

        // contextual help
        'contextual_help'         => array(),

        // typography options
        'enqueue_webfont'         => false,
        'async_webfont'           => false,

        // others
        'output_css'              => false,

        // theme
        'theme'                   => 'dark',

        // external default values
        'defaults'                => array(),

    ));


    CSF::createSection( $prefix, array(

        'id'    => 'general_options', 
        'title' => 'General options',
        'icon'   => 'fa fa-cog',
        'fields' => array(
            
            array(
                'id'        => 'designs',
                'type'      => 'group',
                'title'     => 'Desgins',
                'fields'    => array(
                    array(
                        'id'    => 'title',
                        'type'  => 'text',
                        'title' => 'Title',
                    ),
                    array(
                    'id'    => 'cover-photo',
                    'type'  => 'upload',
                    'title' => 'Cover photo',
                    ),
                    array(
                        'id'        => 'pages',
                        'type'      => 'group',
                        'title'     => 'Pages',
                        'fields'    => array(
                            array(
                            'id'    => 'page-photo',
                            'type'  => 'upload',
                            'title' => 'Page photo',
                            ),
                        ),
                    ),
                ),
            ),

        )

    ));
    
endif;