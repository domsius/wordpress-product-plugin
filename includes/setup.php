<?php

/**
 *
 * https://developer.wordpress.org/reference/functions/wp_enqueue_script/
 * 
 * 
 * https://swiperjs.com/get-started/
 */
add_action( 'wp_enqueue_scripts', 'my_enqueue_scripts' );
function my_enqueue_scripts() {
    wp_enqueue_script(
        'my_swiper_js',
        'https://unpkg.com/swiper/swiper-bundle.min.js',
        array( 'jquery' )
    );
    wp_enqueue_script(
        'my_frontend_js',
        PLUGIN_URL . 'assets/js/frontend.js',
        array( 'jquery' )
    );
    wp_enqueue_style(
        'my_swiper_css',
        'https://unpkg.com/swiper/swiper-bundle.min.css'
    );
    wp_enqueue_style(
        'my_frontend_css',
        PLUGIN_URL . 'assets/css/frontend.css'
    );
}

/**
 *
 *  https://codestarframework.com/
 */
require_once PLUGIN_PATH . 'vendor/codestar-framework-master/codestar-framework.php';
require_once PLUGIN_PATH . 'includes/options.php';



function my_setting( $setting ) {
 
    $settings = get_option( 'plugin_settings' ); 
    if ( isset( $settings[$setting] ) ) :
        return $settings[$setting];
    else :
        return null;
    endif;
}

require_once PLUGIN_PATH . 'includes/woocommerce-process.php';