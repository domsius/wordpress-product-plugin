<?php 

/**
 *  We will use "woocommerce_before_add_to_cart_button"
 *  https://www.businessbloomer.com/woocommerce-visual-hook-guide-single-product-page/
 *  https://developer.wordpress.org/plugins/hooks/
 */

 // With this hook, we are adding to the woocommerce product display page.
add_action( 'woocommerce_before_add_to_cart_button', 'my_covers_list' );
function my_covers_list() {
    ?>
    <p class="form-row">

        <label>Įveskite vaiko vardą:</label>

        <input type="text" class="name" name="name">

    </p><!-- form-row -->
    <p class="form-row">

        <label>Įveskite vaiko gimimo datą: </label>

        <input type="text" class="date-of-brith" name="date_of_brith">

        Galite palikti tuščią.

    </p>
    <div class="placeholder">
            Pasirinkite dizainą:
        </div><!-- form-row -->
    <ul class="my-desings">
    <?php
    /**
     * Here we will now list the covers and pages we set in the setting section of the plugin.
     * We have set our id as desigs in options.php.
     */
    $designs = my_setting( 'designs' );
    foreach ( $designs as $key => $value ) { ?>
        <li class="design-select" data-title="<?php echo $value['title']; ?>">
            <img src="<?php echo $value['cover-photo']; ?>" alt="<?php echo $value['title']; ?>">
            <div class="my-pages">
                <div class="swiper-slide"><img src="<?php echo $value['cover-photo']; ?>"></div>
                <?php foreach ( $value['pages'] as $key => $pages ) { ?>
                    <div class="swiper-slide"><img src="<?php echo $pages['page-photo']; ?>"></div>
                <?php } ?>
            </div>
        </li>
    <?php } ?>
    </ul>
    <input type="hidden" class="selected-design" name="selected_design">
    <div class="clear"></div>
    <p>
    <!-- cover preview -->
        <label>Knygos viršelis:</label>
        
        <div class="swiper-container">
            <div class="swiper-wrapper">
                
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </p><!-- form-row -->
    <p>
    <!-- cover preview -->
        <label>Spalva:</label>

        <input type="color" class="color-picker" name="color_picker">
    </p><!-- form-row -->
    <?php
}

// With this, we transfer our data while being added to the basket.
add_action( 'woocommerce_add_cart_item_data', 'add_to_cart', 10, 2 );
function add_to_cart( $cart_item, $product_id ) {

    // Let's get the data sent to us
    $name = isset( $_POST['name'] ) ? $_POST['name'] : null;
    $date_of_brith = isset( $_POST['date_of_brith'] ) ? $_POST['date_of_brith'] : null;
    $selected_design = isset( $_POST['selected_design'] ) ? $_POST['selected_design'] : null;
    $selected_color = isset( $_POST['color_picker'] ) ? $_POST['color_picker'] : null;
    
    // Then, let's add our data to the basket data variable as a series to add it to the woocommerce's basket.
    $cart_item['my-data'] = array(
        'Name:' => $name,
        'Date of brith:' => $date_of_brith,
        'Selected design:' => $selected_design,
        'Selected color:' => $selected_color
    );

    // Avoid merging items
    $cart_item['unique_key'] = md5( microtime().rand() );

    return $cart_item;
}

// With this, we save our data in the database, and we will list them in the next transaction.
add_action( 'woocommerce_checkout_create_order_line_item', 'create_order_and_add_meta', 20, 4 );
function create_order_and_add_meta( $item, $cart_item_key, $values ) {
    // Is there any data we send, "my-data"?
    if ( isset( $values['my-data'] ) ) {
        // Add as post meta if available
        $item->update_meta_data( 'my-data', $values['my-data'] );
    }
}


add_action( 'woocommerce_after_order_itemmeta', 'list_my_data', 10, 3 );
function list_my_data( $item_id, $item ) {
    $data = $item->get_meta( 'my-data' );
    ?>
    <dl>
    <?php
    foreach ( $data as $key => $value ) {
        ?>
        <div style="float: left;width:100%">
        <dt style="width:auto; float:left; margin-right: 10px"><strong><?php echo $key; ?></strong></dt>
        <dd style="width:auto; float:left; padding: 0; margin: 0"><?php echo $value; ?></dd>
        </div>
        <?php
    }
    ?>
    <dl>
    <?php
}