jQuery(document).ready(function() {
    // Start the slider
    function start_slider() {
        new Swiper('.swiper-container', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }
    // What to do when a design is selected
    jQuery(".design-select").click(function() {
        var content = jQuery(this).children(".my-pages").html();
        jQuery(".swiper-container .swiper-wrapper").html(content);
        jQuery(".swiper-container").show();
        jQuery(".placeholder").hide();
        jQuery(".swiper-container .swiper-wrapper .swiper-slide").eq(0).append('<div class="name">' + jQuery(".name").val() + '</div>');
        jQuery(".swiper-container .swiper-wrapper .swiper-slide").eq(0).append('<div class="date-of-brith">' + jQuery(".date-of-brith").val() + '</div>');
        jQuery(".selected-design").val(jQuery(this).attr("data-title"))
        start_slider();
    });
});